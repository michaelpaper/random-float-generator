#!/bin/bash

./tests

echo """set terminal pngcairo font \"arial,10\" size 500,500
set output 'repartition-values.png'
set boxwidth 0.75
set style fill solid
set yrange [0:*]
set title \"repartition of floats\"
plot \"values-repartition.dat\" using 2:xtic(1) with boxes
""" | gnuplot


echo """set terminal pngcairo font \"arial,10\" size 500,500
set output 'bit-proba.png'
set boxwidth 0.75
set style fill solid
set yrange [0:1]
set title \"probability of each bit\"
plot \"bit-proba.dat\" using 2:xtic(1) with boxes
""" | gnuplot
