#include "random-float-generator.h"
#include <stdlib.h>

#define SIGNIFICANT_MASK 0x007FFFFF


// returns a float f of uniform law over (0,1) (assuming random is uniform)
// it calls random multiple times but does not set srandom, that's up to the
// caller
float random_float()
{
  long seeds[6];
  float f;
  int i;

  for (i=0; i<6; i++)
    seeds[i] = random();
  f = random_float_seeds(seeds);

  return f;
}


// takes as input the parameters of the float, returns the corresponding float
static inline float float_from_parameters(int sign, unsigned char exponent, u_int32_t significant)
{
  float res;
  u_int32_t int_res;
  u_int32_t shifted_exponent;
  u_int32_t shifted_sign;

  shifted_exponent = exponent;
  shifted_exponent <<= 23;
  shifted_sign = sign ? 0x80000000 : 0;
  int_res = shifted_sign | shifted_exponent | significant;
  res = *((float*) &int_res);

  return res;
}


/* the exponent is 126 with proba 1/2, 125 with proba 1/4, 124 with proba
 * 1/8, and so on ...
 * because the proba of having any number from a call to random is 1/2^31
 * and we need events with probabilities as small as 1/2^127, we need 5 random
 * seeds as input
 */
static inline unsigned char random_exponent_from_seeds(long seeds[5])
{
  int i, found;
  u_int32_t mask;
  unsigned char res;

  found = 0;
  res = 126;
  for (i=0; i<5 && !found; i++)
    for (mask=1; mask != 0x80000000 && !found && res < 254; mask<<=1)
      if (seeds[i] & mask)
	found = 1;
      else
	res--;

  return res;
}

// from a seed, generates a float f \in (0,1)
float random_float_seeds(long seeds[6])
{
  float res;
  u_int32_t significant;	  // 23 used bits
  unsigned char exponent;	  // always between 0 and 126 for our purpose
  int sign;			  // always 0 for our purpose

  // the significant field can be generated completely at random
  significant = seeds[5] & SIGNIFICANT_MASK;
  exponent = random_exponent_from_seeds(seeds);
  sign = 0;

  res = float_from_parameters(sign, exponent, significant);

  return res;
}
