#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "random-float-generator.h"

#define DRAWS (1<<25)
#define INTERVALS (1<<5)

/* This test generates DRAWS random floats, and counts the number of those
 * floats that are in [i/INTERVALS, (i+1)/INTERVALS) for 0 <= i < INTERVALS
 * !! this test can quickly become slow when increasing INTERVALS
 * Then, two .dat files are generated
 * values-repartition.dat tells how many values are in each slice
 * each bar should be at the same level
 * bit-repartition.dat tells the probability of each bit of the float being 1
 * bars labeled 0 to 22 should all have proba 1/2
 */

int main()
{
  float f;
  time_t t;
  int i, j, found;
  int count_draws[INTERVALS];
  int count_bit[32];
  float avg, std_deviation;
  FILE* fptr;

  srandom((unsigned) time(&t));

  for (i=0; i<INTERVALS; i++)
    count_draws[i] = 0;
  for (i=0; i<32; i++)
    count_bit[i] = 0;
  for (i=0; i<DRAWS; i++)
  {
    f = random_float();
    found = 0;
    for (j=0; j<INTERVALS && !found; j++)
    {
      if (f < (float) (j+1)/INTERVALS)
      {
	count_draws[j]++;
	found = 1;
      }
    }
    for (j=0; j<32; j++)
      if (*((int*) &f) & (1<<j))
	count_bit[j]++;
  }

  avg = 0;
  for (i=0; i<INTERVALS; i++)
    avg += count_draws[i];
  avg /= INTERVALS;
  for (i=0; i<INTERVALS; i++)
    std_deviation += (count_draws[i] - avg)*(count_draws[i] - avg);
  std_deviation = sqrtf(std_deviation/INTERVALS);

  // write data to values-repartition.dat
  fptr = fopen("values-repartition.dat", "w");
  for (i=0; i<INTERVALS; i++)
    fprintf(fptr, "%g\t%d\n", (float) i/INTERVALS, count_draws[i]);
  fclose(fptr);

  // write data to bit-proba.dat
  fptr = fopen("bit-proba.dat", "w");
  for (i=0; i<32; i++)
    fprintf(fptr, "%d\t%g\n", i, (float) count_bit[i]/DRAWS);
  fclose(fptr);

  return 0;
}
